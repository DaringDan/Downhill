//
//  main.m
//  Downhill
//
//  Created by Sami, Daniel on 2018-01-30.
//  Copyright © 2018 Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
