//
//  AppDelegate.h
//  Downhill
//
//  Created by Sami, Daniel on 2018-01-30.
//  Copyright © 2018 Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

